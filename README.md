# Artfocus Composer Sandbox

This is a sandbox which helps you create a new composer package.

It includes:

* .gitignore rules
* composer.json with private repositories
* shippable.yml for CI
* PHPUnit bootstrap


## New project creation

`$ composer create-project artfocus/composer-sandbox my-app`

Then create a new repository on BitBucket inside `artfocus` account.

Repository name should be lower case, with hyphens instead of spaces.

Then register it in our **[Satis](http://packages.artfocus.biz/)**.
See [our Wiki](https://bitbucket.org/artfocus/artcms2/wiki/Repository) for more information.


## Tests

See [shippable.yml](/shippable.yml) and PHPUnit config.

If you use PhpStorm IDE, you should set paths like on this picture (with your paths):

![i](http://i.imgur.com/0lmcFD7.png)
